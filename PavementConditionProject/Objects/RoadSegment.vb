﻿Public Class RoadSegment
    Inherits CollectionItem
#Region "Member Variables"
    Private m_e1 As Endpoint
    Private m_e2 As Endpoint
    Private m_Damage As Double
    Private m_DaysUntilFixed As Int32 = 0
    Private m_roadType As Int32
    Private m_NumberOfCommutersThisIteration As Int32
#End Region
#Region "Constructors"
    Public Sub New(ByVal rType As Int32)
        MyBase.New()
        m_Damage = 0
        m_roadType = rType
        ObjectsModule.RoadSegments.add(Me)
    End Sub
    Public Sub New(ByVal ep1 As Endpoint, ByVal ep2 As Endpoint, ByVal rType As Int32)
        MyBase.New()
        Endpoint1 = ep1
        Endpoint2 = ep2
        m_Damage = 0
        m_roadType = rType
        ObjectsModule.RoadSegments.add(Me)
    End Sub
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub
    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Properties"
    Public ReadOnly Property RoadSegments As RoadSegments
        Get
            Return ObjectsModule.RoadSegments
        End Get
    End Property
    Public ReadOnly Property RoadType As Int32
        Get
            Return m_roadType
        End Get
    End Property
    Public ReadOnly Property SegmentLength As Double
        Get
            Dim xdiff As Double = Endpoint1.Point.X - Endpoint2.Point.X
            Dim ydiff As Double = Endpoint1.Point.Y - Endpoint2.Point.Y
            Return Math.Sqrt(xdiff * xdiff + ydiff * ydiff)
        End Get
    End Property
    Public ReadOnly Property SegmentCost As Double
        Get
            Return Calculations.SegmentCost(Me)
        End Get
    End Property
    Public Property Endpoint1 As Endpoint
        Get
            Return m_e1
        End Get
        Set(value As Endpoint)
            If Not m_e1 Is Nothing Then
                m_e1.RemoveSegment(Me)
                m_e1 = Nothing
            End If
            m_e1 = value
            If Not m_e1 Is Nothing Then
                m_e1.AddSegment(Me)
            End If
        End Set
    End Property
    Public Property Endpoint2 As Endpoint
        Get
            Return m_e2
        End Get
        Set(value As Endpoint)
            If Not m_e2 Is Nothing Then
                m_e2.RemoveSegment(Me)
                m_e2 = Nothing
            End If
            m_e2 = value
            If Not m_e2 Is Nothing Then
                m_e2.AddSegment(Me)
            End If
        End Set
    End Property
    Public Property RoadDamage As Double
        Get
            Return m_Damage
        End Get
        Set(value As Double)
            m_Damage = value
        End Set
    End Property
    Public Property DaysUntilFixed As Int32
        Get
            Return m_DaysUntilFixed
        End Get
        Set(value As Int32)
            m_DaysUntilFixed = value
        End Set
    End Property
    Public Property NumberOfCommutersThisIteration As Int32
        Get
            Return m_NumberOfCommutersThisIteration
        End Get
        Set(value As Int32)
            m_NumberOfCommutersThisIteration = value
        End Set
    End Property
    Public ReadOnly Property RepairPriority() As Double
        Get
            Return Calculations.RepairPriority(Me)
        End Get
    End Property
    Public ReadOnly Property RepairCost() As Double
        Get
            Return Calculations.RepairCost(Me)
        End Get
    End Property
    Public ReadOnly Property RepairTime() As Int32
        Get
            Return Calculations.RepairTime(Me)
        End Get
    End Property
    Public ReadOnly Property DrawingColor As Color
        Get
            Dim red As Byte = Convert.ToByte(Math.Min(255, m_Damage * 255 / 100))
            Dim green As Byte = Convert.ToByte(Math.Max(0, 255 - m_Damage * 255 / 100))
            Dim blue As Byte = Convert.ToByte(0)
            Return Color.FromRgb(red, green, blue)
        End Get
    End Property
    Public ReadOnly Property DrawingLine As System.Windows.Shapes.Line
        Get
            Return DrawingLine(Endpoint1.Point.X,
                Endpoint1.Point.Y,
                Endpoint2.Point.X,
                Endpoint2.Point.Y)
        End Get
    End Property
    Public ReadOnly Property DrawingLine(ByVal x1 As Double, ByVal y1 As Double, ByVal x2 As Double, ByVal y2 As Double) As System.Windows.Shapes.Line
        Get
            DrawingLine = New System.Windows.Shapes.Line

            DrawingLine.X1 = x1
            DrawingLine.X2 = x2
            DrawingLine.Y1 = y1
            DrawingLine.Y2 = y2

            DrawingLine.Stroke = New SolidColorBrush(DrawingColor)
            Select Case RoadType
                Case 1
                    DrawingLine.StrokeThickness = 4
                Case 2
                    DrawingLine.StrokeThickness = 3
                Case 3
                    DrawingLine.StrokeThickness = 2
            End Select
        End Get
    End Property
    Public ReadOnly Property PointOnLine(ByVal dist As Double) As Point
        Get
            If dist > 1 Then Throw New Exception("PortionSplitPoint: dist > 1")
            If dist < 0 Then Throw New Exception("PortionSplitPoint: dist < 0")
            PointOnLine = New Point(
                Endpoint1.Point.X + (Endpoint2.Point.X - Endpoint1.Point.X) * dist,
                Endpoint1.Point.Y + (Endpoint2.Point.Y - Endpoint1.Point.Y) * dist
                )
        End Get
    End Property
    Public ReadOnly Property PerpVector(ByVal len As Double) As Point
        Get
            Dim xdiff As Double = Me.Endpoint2.Point.X - Me.Endpoint1.Point.X
            Dim ydiff As Double = Me.Endpoint2.Point.Y - Me.Endpoint1.Point.Y
            Dim slen As Double = Me.SegmentLength
            PerpVector = New Point(len * ydiff / slen, -len * xdiff / slen)
        End Get
    End Property
    Public ReadOnly Property RandomPortionSplitPoint(ByVal denominator As Double) As Point
        Get
            Dim numerator As Double = 1
            Dim sign1 As Double = (Math.Floor(Rnd() * 2) * 2 - 1)
            Dim len1 As Double = Math.Sqrt(Rnd() * 64) / 16
            Dim sign2 As Double = (Math.Floor(Rnd() * 2) * 2 - 1)
            Dim len2 As Double = (SegmentLength / denominator) * Math.Sqrt(Rnd() * 64) / 32
            numerator = numerator + sign1 * len1
            Dim distPoint As Point = PointOnLine(numerator / denominator)
            Dim perpPoint As Point = PerpVector(sign2 * len2)
            Return New Point(distPoint.X + perpPoint.X, distPoint.Y + perpPoint.Y)
        End Get
    End Property
#End Region
#Region "Methods"
    Public Sub DamageRoadSegment()
        Calculations.DamageRoadSegment(Me)
    End Sub
    Public Sub Shift(ByVal xShift As Double, ByVal yShift As Double)
        Dim origLen As Double = Me.SegmentLength
        Dim x1a As Double = Me.Endpoint1.Point.X
        Dim x1b As Double = Me.Endpoint2.Point.X
        Dim y1a As Double = Me.Endpoint1.Point.Y
        Dim y1b As Double = Me.Endpoint2.Point.Y

        Dim ep1 As Endpoint = Endpoint.NewEndpointShifted(Me.Endpoint1, xShift, yShift)
        Dim ep2 As Endpoint = Endpoint.NewEndpointShifted(Me.Endpoint2, xShift, yShift)
        Me.Endpoint1 = ep1
        Me.Endpoint2 = ep2

        If Math.Abs(origLen - Me.SegmentLength) > 0.00001 Then
            Throw New Exception("Length doesn't match up after shifting.")
        End If
    End Sub
    Public Function FixShift(ByVal maxX As Double, ByVal maxY As Double) As Boolean
        FixShift = False

        Dim x1a As Double = Me.Endpoint1.Point.X
        Dim x1b As Double = Me.Endpoint2.Point.X

        If (x1a <= 0 And x1b <= 0) And (x1a < 0 Or x1b < 0) Then
            Shift(maxX, 0)
            FixShift = True
        ElseIf (x1a >= maxX And x1b >= maxX) And (x1a > maxX Or x1b > maxX) Then
            Shift(-maxX, 0)
            FixShift = True
        End If

        Dim y1a As Double = Me.Endpoint1.Point.Y
        Dim y1b As Double = Me.Endpoint2.Point.Y

        If (y1a <= 0 And y1b <= 0) And (y1a < 0 Or y1b < 0) Then
            Shift(0, maxY)
            FixShift = True
        ElseIf (y1a >= maxY And y1b >= maxY) And (y1a > maxY Or y1b > maxY) Then
            Shift(0, -maxY)
            FixShift = True
        End If

        x1a = Me.Endpoint1.Point.X
        x1b = Me.Endpoint2.Point.X
        y1a = Me.Endpoint1.Point.Y
        y1b = Me.Endpoint2.Point.Y

        If (x1a <= 0 And x1b <= 0) And (x1a < 0 Or x1b < 0) Then
            Throw New Exception("Failed to shift.")
        ElseIf (x1a >= maxX And x1b >= maxX) And (x1a > maxX Or x1b > maxX) Then
            Throw New Exception("Failed to shift.")
        ElseIf (y1a <= 0 And y1b <= 0) And (y1a < 0 Or y1b < 0) Then
            Throw New Exception("Failed to shift.")
        ElseIf (y1a >= maxY And y1b >= maxY) And (y1a > maxY Or y1b > maxY) Then
            Throw New Exception("Failed to shift.")
        End If
    End Function
    Public Function SplitOnEdgeCrossings(ByVal maxX As Double, ByVal maxY As Double) As Boolean



        Dim x1a As Double = Me.Endpoint1.Point.X
        Dim x1b As Double = Me.Endpoint2.Point.X

        Dim y1a As Double = Me.Endpoint1.Point.Y
        Dim y1b As Double = Me.Endpoint2.Point.Y

        Dim slope1 As Double = (y1a - y1b) / (x1a - x1b)
        Dim yzero1 As Double = y1a - slope1 * x1a

        Dim y1aEx As Double = yzero1 + slope1 * x1a
        Dim y1bEx As Double = yzero1 + slope1 * x1b



        Dim origLen As Double = Me.SegmentLength
        Dim arr(2) As RoadSegment
        Dim splitPoint As Point = Nothing
        If x1a < 0 Or x1b < 0 Then
            splitPoint = SegmentLeftEdgeCrossings(yzero1, slope1, maxX, maxY)
        ElseIf x1a > maxX Or x1b > maxX Then
            splitPoint = SegmentRightEdgeCrossings(yzero1, slope1, maxX, maxY)
        ElseIf y1a < 0 Or y1b < 0 Then
            splitPoint = SegmentTopEdgeCrossings(yzero1, slope1, maxX, maxY)
        ElseIf y1a > maxY < 0 Or y1b > maxY Then
            splitPoint = SegmentBottomEdgeCrossings(yzero1, slope1, maxX, maxY)
        Else
            Return False
        End If
        If Me.Endpoint1.X = splitPoint.X And Me.Endpoint1.Y = splitPoint.Y Then
            Return False
        ElseIf Me.Endpoint2.X = splitPoint.X And Me.Endpoint2.Y = splitPoint.Y Then
            Return False
        End If
        arr = Split(splitPoint)
        If Not arr(0).Endpoint1 Is Me.Endpoint1 Then
            Throw New Exception("Endpoint 1 does not match up.")
        ElseIf Not arr(0).Endpoint2 Is arr(1).Endpoint1 Then
            Throw New Exception("Split endpoints don't match up.")
        ElseIf Not arr(1).Endpoint2 Is Me.Endpoint2 Then
            Throw New Exception("Endpoint 2 does not match up.")
        ElseIf Math.Abs(arr(0).SegmentLength + arr(1).SegmentLength - origLen) > 0.00001 Then
            Throw New Exception("Length doesn't match up after splitting.")
        ElseIf arr(0).SegmentLength = 0 Or arr(1).SegmentLength = 0 Then
            Throw New Exception("Created a 0 length segment.")
        End If
        arr(0).FixShift(maxX, maxY)
        arr(1).FixShift(maxX, maxY)
        Return True
    End Function
    Public Function Split(ByVal ep As Endpoint) As RoadSegment()
        Dim arr(2) As RoadSegment
        arr(0) = New RoadSegment(Endpoint1, ep, RoadType)
        arr(1) = New RoadSegment(ep, Endpoint2, RoadType)
        Split = arr
        RoadSegments.Remove(Me)
        Me.Dispose()
    End Function
    Public Function Split(ByVal p As Point) As RoadSegment()
        Return Split(New Endpoint(p))
    End Function
    Public Function Offshoot(ByVal dist As Double, ByVal length As Double) As RoadSegment
        Dim p1 As Point = Me.PointOnLine(dist)
        Dim vector As Point = PerpVector(length)
        Dim ep1 As New Endpoint(p1)
        Dim p2 As Point = New Point(p1.X + vector.X, p1.Y + vector.Y)
        Dim ep2 As New Endpoint(p2)
        Dim rs1 As RoadSegment() = Split(ep1)
        Offshoot = New RoadSegment(ep1, ep2, RoadType + 1)
    End Function
    Public Shared Function SegmentLeftEdgeCrossings(ByVal yzero1 As Double, ByVal slope1 As Double, ByVal maxX As Double, ByVal maxY As Double) As Point
        Return New Point(0, yzero1)
    End Function
    Public Shared Function SegmentRightEdgeCrossings(ByVal yzero1 As Double, ByVal slope1 As Double, ByVal maxX As Double, ByVal maxY As Double) As Point
        Return New Point(maxX, yzero1 + slope1 * maxX)
    End Function
    Public Shared Function SegmentBottomEdgeCrossings(ByVal yzero1 As Double, ByVal slope1 As Double, ByVal maxX As Double, ByVal maxY As Double) As Point
        Return New Point((maxY - yzero1) / slope1, maxY)
    End Function
    Public Shared Function SegmentTopEdgeCrossings(ByVal yzero1 As Double, ByVal slope1 As Double, ByVal maxX As Double, ByVal maxY As Double) As Point
        Return New Point((0 - yzero1) / slope1, 0)
    End Function
#End Region
End Class
