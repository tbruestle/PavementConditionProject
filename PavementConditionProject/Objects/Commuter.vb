﻿Public Class Commuter
    Inherits CollectionItem
#Region "Member Variables"
    Dim m_origin As Endpoint
    Dim m_destination As Endpoint
    Dim m_bestRoute As String = Nothing
    Dim m_lastRoute As String = Nothing
    Dim m_lastRouteCost As Double = -1
    Dim m_lastRouteLength As Double = -1
    Dim m_lastRouteList As List(Of RoadSegment) = Nothing
    Dim m_currentBestRoute As String = Nothing
    Dim m_bestRoutePathLength As Double = -1
    Dim m_currentBestRoutePathLength As Double = -1
    Dim m_bestRoutePathCost As Double = -1
    Dim m_currentBestRoutePathCost As Double = -1
    Dim m_historicalCost As Double = 0
#End Region
#Region "Constructors"
    Public Sub New()
        MyBase.New()
        ObjectsModule.Commuters.Add(Me)
    End Sub
    Public Sub New(ByVal rs1 As RoadSegment, ByVal rs2 As RoadSegment, ByVal portion1 As Double, ByVal portion2 As Double)
        MyBase.New()
        Dim p1 As Point = rs1.PointOnLine(portion1)
        Dim p2 As Point = rs2.PointOnLine(portion2)
        Dim rsnew1() As RoadSegment
        Dim rsnew2() As RoadSegment
        rsnew1 = rs1.Split(p1)
        If Not rs1 Is rs2 Then
            rsnew2 = rs2.Split(p2)
        ElseIf portion1 < portion2 Then
            rsnew2 = rsnew1(1).Split(p2)
        Else
            rsnew2 = rsnew1(0).Split(p2)
        End If
        m_origin = rsnew1(0).Endpoint2
        m_destination = rsnew2(0).Endpoint2
        m_bestroute = Nothing
        ObjectsModule.Commuters.Add(Me)
    End Sub
    Public Shared Function RandomNew()
        Dim cnt As Int32 = ObjectsModule.RoadSegments.Count
        If cnt < 1 Then
            Throw New Exception("Not enough road segments.")
        End If
        Dim i1 As Int32 = Convert.ToInt32(Math.Round(Math.Floor(Rnd() * cnt), 0))
        Dim i2 As Int32 = Convert.ToInt32(Math.Round(Math.Floor(Rnd() * cnt), 0))

        Dim rs1 As RoadSegment = ObjectsModule.RoadSegments(i1)
        Dim rs2 As RoadSegment = ObjectsModule.RoadSegments(i2)

        Dim portion1 As Double = Rnd()
        Dim portion2 As Double = Rnd()

        Return New Commuter(rs1, rs2, portion1, portion2)
    End Function
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub
    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Properties"
    Public ReadOnly Property Origin As Endpoint
        Get
            Return m_origin
        End Get
    End Property
    Public ReadOnly Property Destination As Endpoint
        Get
            Return m_destination
        End Get
    End Property
    Public ReadOnly Property BestRoute As String
        Get
            InitBestRoute()
            BestRoute = m_bestRoute
        End Get
    End Property
    Public ReadOnly Property CurrentBestRoute As String
        Get
            InitCurrentBestRoute()
            CurrentBestRoute = m_currentBestRoute
        End Get
    End Property
    Public Function PathAsList(ByVal pathStr As String) As List(Of RoadSegment)
        PathAsList = New List(Of RoadSegment)

        Dim segs() As String = pathStr.Split(",")
        For Each seg As String In segs
            Dim i As Int32 = Convert.ToInt32(seg)
            If i >= 0 Then
                PathAsList.Add(ObjectsModule.RoadSegments(i))
            End If
        Next
    End Function
    Public ReadOnly Property PathCost(ByVal segs As List(Of RoadSegment))
        Get
            PathCost = 0
            For Each seg As RoadSegment In segs
                PathCost += seg.SegmentCost
            Next
        End Get
    End Property
    Public ReadOnly Property PathCost(ByVal pathStr As String)
        Get
            Return PathCost(PathAsList(pathStr))
        End Get
    End Property
    Public ReadOnly Property PathLength(ByVal segs As List(Of RoadSegment))
        Get
            PathLength = 0
            For Each seg As RoadSegment In segs
                PathLength += seg.SegmentLength
            Next
        End Get
    End Property
    Public ReadOnly Property PathLength(ByVal pathStr As String)
        Get
            Return PathLength(PathAsList(pathStr))
        End Get
    End Property
    Public ReadOnly Property BestRouteLength
        Get
            InitBestRoute()
            BestRouteLength = m_bestRoutePathLength
        End Get
    End Property
    Public ReadOnly Property CurrentBestRouteLength
        Get
            InitCurrentBestRoute()
            CurrentBestRouteLength = m_currentBestRoutePathLength
        End Get
    End Property
    Public ReadOnly Property BestRouteCost
        Get
            InitBestRouteCost()
            BestRouteCost = m_bestRoutePathCost
        End Get
    End Property
    Public ReadOnly Property CurrentBestRouteCost As Double
        Get
            InitCurrentBestRouteCost()
            CurrentBestRouteCost = m_currentBestRoutePathCost
        End Get
    End Property
    Public ReadOnly Property LastRoute As String
        Get
            Return m_lastRoute
        End Get
    End Property
    Public ReadOnly Property LastRouteHasConstruction As Boolean
        Get
            If m_lastRouteList Is Nothing Then Return False
            Return LastRouteList.Where(Function(p) p.DaysUntilFixed > 0).Count > 0
        End Get
    End Property
    Public ReadOnly Property LastRouteList As List(Of RoadSegment)
        Get
            Return m_lastRouteList
        End Get
    End Property
    Public ReadOnly Property LastRouteContains As List(Of RoadSegment)
        Get
            Return m_lastRouteList
        End Get
    End Property
    Public Property LastRouteLength As Double
        Get
            Return m_lastRouteLength
        End Get
        Set(value As Double)
            m_lastRouteLength = value
        End Set
    End Property
    Public Property LastRouteCost As Double
        Get
            Return m_lastRouteCost
        End Get
        Set(value As Double)
            m_lastRouteCost = value
        End Set
    End Property
    Public ReadOnly Property RouteEfficiency As Double
        Get
            Return LastRouteCost / BestRouteLength
        End Get
    End Property
    Public ReadOnly Property HistoricalCost As Double
        Get
            Return m_historicalCost
        End Get
    End Property
#End Region
#Region "Methods"
    Public Sub UpdateHistoricalCost()
        m_historicalCost += LastRouteCost
    End Sub
    Public Sub InitBestRoute()
        If m_bestRoute Is Nothing Then
            If Not ShortestPath.IsInit Then
                ShortestPath.Update()
            End If
            m_bestRoute = ShortestPath.ShortestPath(
                ObjectsModule.Endpoints.IndexOf(Origin),
                ObjectsModule.Endpoints.IndexOf(Destination)
                )
            m_currentBestRoute = m_bestRoute
            m_bestRoutePathLength = PathLength(m_bestRoutePathLength)
            m_currentBestRoutePathLength = m_bestRoutePathLength
            UpdateDesiredRoute()
        End If
    End Sub
    Public Sub InitCurrentBestRoute()
        If m_currentBestRoute Is Nothing Then
            If Not ShortestPath.IsInit Then
                ShortestPath.Update()
            End If
            m_currentBestRoute = ShortestPath.ShortestPath(
                ObjectsModule.Endpoints.IndexOf(Origin),
                ObjectsModule.Endpoints.IndexOf(Destination)
                )
            m_currentBestRoutePathLength = PathLength(m_currentBestRoutePathLength)
        End If
    End Sub
    Public Sub InitBestRouteCost()
        InitBestRoute()
        If m_bestRoutePathCost = -1 Then
            m_bestRoutePathCost = PathCost(BestRoute)
        End If
    End Sub
    Public Sub InitCurrentBestRouteCost()
        InitCurrentBestRoute()
        If m_currentBestRoutePathCost = -1 Then
            m_currentBestRoutePathCost = PathCost(CurrentBestRoute)
        End If
    End Sub
    Public Sub ClearCurrentBestRoute()
        m_currentBestRoute = Nothing
        m_currentBestRoutePathLength = -1
        m_currentBestRoutePathCost = -1
    End Sub
    Public Sub ClearBestRoutePathCost()
        m_bestRoutePathCost = -1
    End Sub
    Public Sub ClearCurrentBestRoutePathCost()
        m_currentBestRoutePathCost = -1
    End Sub
    Public Sub UpdateDesiredRoute()
        ClearCurrentBestRoutePathCost()
        ClearBestRoutePathCost()
        If Calculations.UpdateDesiredRoute(Me) Then
            ClearCurrentBestRoute()
        End If
        m_lastRoute = CurrentBestRoute
        m_lastRouteList = PathAsList(m_lastRoute)
        m_lastRouteLength = PathLength(m_lastRouteList)
    End Sub
    Public Sub Commute()
        Calculations.Commute(Me)
    End Sub
#End Region
End Class
