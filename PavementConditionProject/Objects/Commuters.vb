﻿Public Class Commuters
    Inherits ItemCollection(Of Commuter)
#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub
    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Properties"
    Public ReadOnly Property HistoricalCost As Double
        Get
            HistoricalCost = 0
            Dim cnt As Int32 = Me.Count
            For i As Int32 = 0 To cnt - 1
                HistoricalCost += Me(i).HistoricalCost()
            Next
        End Get
    End Property
    Public ReadOnly Property LastCost As Double
        Get
            LastCost = 0
            Dim cnt As Int32 = Me.Count
            For i As Int32 = 0 To cnt - 1
                LastCost += Me(i).LastRouteCost()
            Next
        End Get
    End Property
    Public ReadOnly Property RouteEfficiency As Double
        Get
            RouteEfficiency = 0
            Dim cnt As Int32 = Me.Count
            For i As Int32 = 0 To cnt - 1
                RouteEfficiency += Me(i).RouteEfficiency()
            Next
        End Get
    End Property
    Public ReadOnly Property AverageRouteEfficiency As Double
        Get
            AverageRouteEfficiency = RouteEfficiency / Me.Count
        End Get
    End Property
#End Region
#Region "Methods"
    Public Sub RandomNew(ByVal cnt As Int32)
        For i As Int32 = 0 To cnt - 1
            Commuter.RandomNew()
        Next
    End Sub
    Public Sub InitBestRoutes()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).InitBestRoute()
        Next
    End Sub
    Public Sub ClearCurrentBestRoutes()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).ClearCurrentBestRoute()
        Next
    End Sub
    Public Sub ClearCurrentBestRoutePathCosts()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).ClearCurrentBestRoutePathCost()
        Next
    End Sub
    Public Sub ClearBestRoutePathCosts()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).ClearBestRoutePathCost()
        Next
    End Sub
    Public Sub UpdateDesiredRoutes()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).UpdateDesiredRoute()
        Next
    End Sub
    Public Sub Commute()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).Commute()
        Next
    End Sub
#End Region
End Class
