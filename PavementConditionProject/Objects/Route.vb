﻿Public Class Route
    Inherits CollectionItem
#Region "Member Variables"

#End Region
#Region "Constructors"
    Public Sub New()
        MyBase.New()
        ObjectsModule.Routes.add(Me)
    End Sub
    Public Sub New(ByRef rs As RoadSegments, ByVal ep1 As Endpoint, ByVal ep2 As Endpoint, ByVal epCnt As Int32, ByVal RoadType As Int32)
        MyBase.New()
        Dim rs1 As New RoadSegment(ep1, ep2, RoadType)
        For i As Int32 = 1 To epCnt - 2
            rs1 = rs1.Split(New Endpoint(rs1.RandomPortionSplitPoint(epCnt - i)))(1)
        Next
        ObjectsModule.Routes.add(Me)
    End Sub
    Public Shared Function NewSpanningRoute(ByRef rs As RoadSegments, ByVal isVertical As Boolean, ByVal maxX As Double, ByVal maxY As Double, ByVal epCnt As Int32) As Route
        Dim ep1 As Endpoint
        Dim ep2 As Endpoint
        If isVertical Then
            Dim x = Rnd() * maxX
            ep1 = New Endpoint(x, 0)
            ep2 = New Endpoint(x, maxY)
        Else
            Dim y = Rnd() * maxY
            ep1 = New Endpoint(0, y)
            ep2 = New Endpoint(maxX, y)
        End If
        Return New Route(rs, ep1, ep2, epCnt, 1)
    End Function
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub

    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
End Class
