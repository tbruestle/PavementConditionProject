﻿Public Class Endpoint
    Inherits CollectionItem
#Region "Member Variables"
    Private m_pt As Point
    Private m_segments As List(Of RoadSegment)
    Private m_equivEndpoints As List(Of Endpoint)
#End Region
#Region "Constructors"
    Public Sub New()
        MyBase.New()
        m_segments = New List(Of RoadSegment)
        m_pt = New Point
        m_equivEndpoints = New List(Of Endpoint) From {Me}
        ObjectsModule.Endpoints.Add(Me)
    End Sub
    Public Sub New(ByVal x As Double, ByVal y As Double)
        MyBase.New()
        m_segments = New List(Of RoadSegment)
        m_pt = New Point(x, y)
        m_equivEndpoints = New List(Of Endpoint) From {Me}
        ObjectsModule.Endpoints.Add(Me)
    End Sub
    Public Sub New(ByVal p As Point)
        MyBase.New()
        m_segments = New List(Of RoadSegment)
        m_pt = p
        m_equivEndpoints = New List(Of Endpoint) From {Me}
        ObjectsModule.Endpoints.Add(Me)
    End Sub
    Public Shared Function NewEndpointShifted(ByVal ep As Endpoint, ByVal xShift As Double, ByVal yShift As Double)
        NewEndpointShifted = New Endpoint(New Point(ep.X + xShift, ep.Y + yShift))
        ep.EquivalentPoints.Add(NewEndpointShifted)
        NewEndpointShifted.EquivalentPoints = ep.EquivalentPoints
    End Function
    'Public Shared Function RandomPoint(ByVal maxX As Double, ByVal maxY As Double) As Endpoint
    '    RandomPoint = New Endpoint
    '    RandomPoint.Point = New Point(Rnd() * maxX, Rnd() * maxY)
    'End Function
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()
        m_pt = Nothing
    End Sub
    Protected Overrides Sub DisposeUnmanaged()
        m_segments.Clear()
        m_segments = Nothing
    End Sub
#End Region
#Region "Properties"
    Public Property EquivalentPoints As List(Of Endpoint)
        Get
            Return m_equivEndpoints
        End Get
        Set(value As List(Of Endpoint))
            m_equivEndpoints = value
        End Set
    End Property
    Public ReadOnly Property RoadSegments As List(Of RoadSegment)
        Get
            Return m_segments
        End Get
    End Property
    Public Property Point As Point
        Get
            Return m_pt
        End Get
        Set(value As Point)
            m_pt = value
        End Set
    End Property
    Public ReadOnly Property X As Double
        Get
            Return Point.X
        End Get
    End Property
    Public ReadOnly Property Y As Double
        Get
            Return Point.Y
        End Get
    End Property
#End Region
#Region "Methods"
    Public Shared Sub MergeEquivalentPoints(ByVal ep1 As Endpoint, ByVal ep2 As Endpoint, ByVal maxX As Double, ByVal maxY As Double)
        Dim x1 As Double = ep1.X Mod maxX
        Dim x2 As Double = ep2.X Mod maxX
        If x1 <> x2 Then Return

        Dim y1 As Double = ep1.Y Mod maxY
        Dim y2 As Double = ep2.Y Mod maxY
        If y1 <> y2 Then Return

        If ep1.EquivalentPoints Is ep2.EquivalentPoints Then Return

        ep1.EquivalentPoints.AddRange(ep2.EquivalentPoints)
        ep2.EquivalentPoints.Clear()
        ep2.EquivalentPoints = Nothing
        ep2.EquivalentPoints = ep1.EquivalentPoints
    End Sub
    Public Function AddSegment(ByVal rs As RoadSegment) As Boolean
        If m_segments.Contains(rs) Then
            Return False
        Else
            m_segments.Add(rs)
            Return True
        End If
    End Function
    Public Function RemoveSegment(ByVal rs As RoadSegment) As Boolean
        If Not m_segments.Contains(rs) Then
            Return False
        Else
            m_segments.Remove(rs)
            Return True
        End If
    End Function
#End Region
End Class