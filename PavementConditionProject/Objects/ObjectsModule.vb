﻿Module ObjectsModule
#Region "Member Variables"
    Dim m_rs As RoadSegments
    Dim m_eps As Endpoints
    Dim m_routes As Routes
    Dim m_commuters As Commuters
#End Region
#Region "Properties"
    Public ReadOnly Property RoadSegments As RoadSegments
        Get
            If m_rs Is Nothing Then
                m_rs = New RoadSegments()
            End If
            Return m_rs
        End Get
    End Property
    Public Sub DrawAll(ByRef cvs As Canvas, ByVal xmax As Double, ByVal ymax As Double)
        RoadSegments.DrawAll(cvs, xmax, ymax)
    End Sub
    Public Sub LoadVariables(ByRef window As MainWindow)
        window.lblIteration.Content = Calculations.IterationCount
        window.lblHistoricalCost.Content = Commuters.HistoricalCost
    End Sub
    Public ReadOnly Property Routes As Routes
        Get
            If m_routes Is Nothing Then
                m_routes = New Routes
            End If
            Return m_routes
        End Get
    End Property
    Public ReadOnly Property Endpoints As Endpoints
        Get
            If m_eps Is Nothing Then
                m_eps = New Endpoints
            End If
            Return m_eps
        End Get
    End Property
    Public ReadOnly Property Commuters As Commuters
        Get
            If m_commuters Is Nothing Then
                m_commuters = New Commuters
            End If
            Return m_commuters
        End Get
    End Property
#End Region
End Module
