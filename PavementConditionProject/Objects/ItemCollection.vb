﻿Public MustInherit Class ItemCollection(Of T As CollectionItem)
    Implements IList(Of T)
    Implements IDisposable

#Region "Member Variables"
    Dim lst As List(Of T)
#End Region
#Region "Constructors"
    Public Sub New()
        lst = New List(Of T)
    End Sub
#End Region
#Region "Destructors"
    Protected MustOverride Sub DisposeManaged()
    Protected MustOverride Sub DisposeUnmanaged()
#End Region
#Region "IDisposable Support"
    Private disposedValue As Boolean

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                DisposeManaged()
            End If
            For Each i As T In lst
                i.Dispose()
            Next
            lst.Clear()
            lst = Nothing
            DisposeUnmanaged()
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region
#Region "IList Support"
    Public Sub Add(item As T) Implements ICollection(Of T).Add
        lst.Add(item)
    End Sub
    Public Sub Clear() Implements ICollection(Of T).Clear
        lst.Clear()
    End Sub
    Public Function Contains(item As T) As Boolean Implements ICollection(Of T).Contains
        Return lst.Contains(item)
    End Function
    Public Sub CopyTo(array() As T, arrayIndex As Integer) Implements ICollection(Of T).CopyTo
        lst.CopyTo(array, arrayIndex)
    End Sub
    Public ReadOnly Property Count As Integer Implements ICollection(Of T).Count
        Get
            Return lst.Count
        End Get
    End Property
    Public ReadOnly Property IsReadOnly As Boolean Implements ICollection(Of T).IsReadOnly
        Get
            Return False
        End Get
    End Property
    Public Function Remove(item As T) As Boolean Implements ICollection(Of T).Remove
        Return lst.Remove(item)
    End Function
    Public Function GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
        Return lst.GetEnumerator()
    End Function
    Public Function IndexOf(item As T) As Integer Implements IList(Of T).IndexOf
        Return lst.IndexOf(item)
    End Function
    Public Sub Insert(index As Integer, item As T) Implements IList(Of T).Insert
        lst.Insert(index, item)
    End Sub
    Default Public Property Item(index As Integer) As T Implements IList(Of T).Item
        Get
            Return lst.Item(index)
        End Get
        Set(value As T)
            value = lst.Item(index)
        End Set
    End Property
    Public Sub RemoveAt(index As Integer) Implements IList(Of T).RemoveAt
        lst.RemoveAt(index)
    End Sub
    Public Function GetEnumerator1() As IEnumerator Implements IEnumerable.GetEnumerator
        Return lst.GetEnumerator()
    End Function
#End Region
End Class
