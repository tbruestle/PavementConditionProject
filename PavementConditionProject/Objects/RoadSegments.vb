﻿Public Class RoadSegments
    Inherits ItemCollection(Of RoadSegment)
#Region "Member Variables"
    Private m_routes
#End Region
#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub
    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Properties"
    Public ReadOnly Property Routes As Routes
        Get
            Return ObjectsModule.Routes
        End Get
    End Property
#End Region
#Region "Methods"
    Public Sub DamageRoadSegments()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).DamageRoadSegment()
        Next
    End Sub
    Public Sub DrawAll(ByRef cvs As Canvas, ByVal xmax As Double, ByVal ymax As Double)
        cvs.Children.Clear()
        For Each rs As RoadSegment In Me
            Dim dl As Line = rs.DrawingLine
            cvs.Children.Add(dl)
            ' If dl.
        Next ' Then
    End Sub
    Public Sub SplitIntersections()
        Dim cnt As Int32 = Me.Count
        Dim i As Int32 = 0
        While i < cnt
            For j As Int32 = i + 1 To cnt - 1

                Dim seg1 As RoadSegment = Me(i)
                Dim seg2 As RoadSegment = Me(j)

                If seg1.Endpoint1 Is seg2.Endpoint1 Then Continue For '' Meet on endpoints
                If seg1.Endpoint2 Is seg2.Endpoint1 Then Continue For '' Meet on endpoints
                If seg1.Endpoint1 Is seg2.Endpoint2 Then Continue For '' Meet on endpoints
                If seg1.Endpoint2 Is seg2.Endpoint2 Then Continue For '' Meet on endpoints

                Dim p1 As Point? = SegmentIntersections(seg1, seg2)
                If p1 Is Nothing Then
                    Continue For
                End If
                Dim p As Point = p1.GetValueOrDefault


                Dim minx1 As Double = Math.Min(Me(i).Endpoint1.Point.X, Me(i).Endpoint2.Point.X)
                Dim maxx1 As Double = Math.Max(Me(i).Endpoint1.Point.X, Me(i).Endpoint2.Point.X)
                Dim minx2 As Double = Math.Min(Me(j).Endpoint1.Point.X, Me(j).Endpoint2.Point.X)
                Dim maxx2 As Double = Math.Max(Me(j).Endpoint1.Point.X, Me(j).Endpoint2.Point.X)

                Dim miny1 As Double = Math.Min(Me(i).Endpoint1.Point.Y, Me(i).Endpoint2.Point.Y)
                Dim maxy1 As Double = Math.Max(Me(i).Endpoint1.Point.Y, Me(i).Endpoint2.Point.Y)
                Dim miny2 As Double = Math.Min(Me(j).Endpoint1.Point.Y, Me(j).Endpoint2.Point.Y)
                Dim maxy2 As Double = Math.Max(Me(j).Endpoint1.Point.Y, Me(j).Endpoint2.Point.Y)

                Dim cross1 As Boolean = False
                Dim cross2 As Boolean = False

                cross1 = cross1 Or (minx1 < p.X And p.X < maxx1)
                cross1 = cross1 Or (miny1 < p.Y And p.Y < maxy1)
                cross2 = cross2 Or (minx2 < p.X And p.X < maxx2)
                cross2 = cross2 Or (miny2 < p.Y And p.Y < maxy2)

                If cross1 Then
                    seg1.Split(New Endpoint(p))
                End If
                If cross2 Then
                    seg2.Split(New Endpoint(p))
                End If
                If Not cross1 And Not cross2 Then
                    Continue For
                End If
                cnt = Me.Count
                Continue While
            Next
            i = i + 1
        End While
    End Sub
    Public Sub CreateOffshootsOfType(ByVal offshootCount As Int32, ByVal maxLength As Double, ByVal RoadType As Int32)
        Dim cnt As Int32 = Me.Count
        Dim ptrs As New List(Of RoadSegment)
        If offshootCount > cnt Then
            Throw New Exception("Too many offshoots.")
        End If
        For j As Int32 = 0 To cnt - 1
            If Me(j).RoadType = RoadType - 1 Then ptrs.Add(Me(j))
        Next
        Dim ptrscnt As Int32 = ptrs.Count
        For j As Int32 = 0 To offshootCount - 1
            Dim i As Int32 = Convert.ToInt32(Math.Round(Math.Floor(Rnd() * ptrscnt)))
            Dim rs As RoadSegment = ptrs(i)
            rs.Offshoot(Rnd(), (Rnd() - 0.5) * 2 * maxLength)
            ptrs.Remove(rs)
            ptrscnt -= 1
        Next
    End Sub
    Public Sub SplitOnEdgeCrossings(ByVal maxX As Double, ByVal maxY As Double)
        Dim cnt As Int32 = Me.Count
        For j As Int32 = 0 To cnt - 1
            Me(j).FixShift(maxX:=maxX, maxY:=maxY)
        Next
        Dim i As Int32 = 0
        While i < cnt

            Dim seg1 As RoadSegment = Me(i)
            If Me(i).SplitOnEdgeCrossings(maxX:=maxX, maxY:=maxY) Then
                cnt = Me.Count
                Continue While
            End If
            i = i + 1
        End While
    End Sub
    Public Shared Function SegmentsHaveIntersection(ByVal seg1 As RoadSegment, ByVal seg2 As RoadSegment) As Boolean
        If seg1.Endpoint1 Is seg2.Endpoint1 Then Return False '' Meet on endpoints
        If seg1.Endpoint2 Is seg2.Endpoint1 Then Return False '' Meet on endpoints
        If seg1.Endpoint1 Is seg2.Endpoint2 Then Return False '' Meet on endpoints
        If seg1.Endpoint2 Is seg2.Endpoint2 Then Return False '' Meet on endpoints


        Dim p1 As Point? = SegmentIntersections(seg1, seg2)
        If p1 Is Nothing Then
            Return False
        End If
        Dim p As Point = p1.GetValueOrDefault


        Dim minx As Double = Math.Min(seg1.Endpoint1.Point.X, seg1.Endpoint2.Point.X)
        Dim maxx As Double = Math.Max(seg1.Endpoint1.Point.X, seg1.Endpoint2.Point.X)
        If minx <= p.X And p.X <= maxx Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function SegmentIntersections(ByVal seg1 As RoadSegment, ByVal seg2 As RoadSegment) As Point?
        Dim x1a As Double = seg1.Endpoint1.Point.X
        Dim x1b As Double = seg1.Endpoint2.Point.X
        Dim x2a As Double = seg2.Endpoint1.Point.X
        Dim x2b As Double = seg2.Endpoint2.Point.X

        Dim y1a As Double = seg1.Endpoint1.Point.Y
        Dim y1b As Double = seg1.Endpoint2.Point.Y
        Dim y2a As Double = seg2.Endpoint1.Point.Y
        Dim y2b As Double = seg2.Endpoint2.Point.Y


        Dim slope1 As Double? = If((x1a - x1b) = 0, Nothing, (y1a - y1b) / (x1a - x1b))
        Dim slope2 As Double? = If((x2a - x2b) = 0, Nothing, (y2a - y2b) / (x2a - x2b))

        Dim yzero1 As Double? = If(slope1 Is Nothing, Nothing, y1a - slope1 * x1a)
        Dim yzero2 As Double? = If(slope2 Is Nothing, Nothing, y2a - slope2 * x2a)

        If (x1a = x1b) And (x2a = x2b) Then
            '' Parallel Lines
            Return Nothing
        ElseIf (x1a = x1b) Then
            Dim xIntersection As Double = x1a
            Dim yIntersection As Double = slope2 * xIntersection + yzero2
            Return New Point(xIntersection, yIntersection)
        ElseIf (x2a = x2b) Then
            Dim xIntersection As Double = x2a
            Dim yIntersection As Double = slope1 * xIntersection + yzero1
            Return New Point(xIntersection, yIntersection)
        Else
            Dim y1aEx As Double = yzero1 + slope1 * x1a
            Dim y1bEx As Double = yzero1 + slope1 * x1b
            Dim y2aEx As Double = yzero2 + slope2 * x2a
            Dim y2bEx As Double = yzero2 + slope2 * x2b


            Dim xIntersection As Double = (yzero2 - yzero1) / (slope1 - slope2)
            Dim yIntersection As Double = slope1 * xIntersection + yzero1

            Return New Point(xIntersection, yIntersection)
        End If
    End Function
    Public Sub ResetNumberOfCommutersThisIteration()
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Me(i).NumberOfCommutersThisIteration = 0
        Next
    End Sub
#End Region
End Class
