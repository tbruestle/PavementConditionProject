﻿Public Class Endpoints
    Inherits ItemCollection(Of Endpoint)
#Region "Constructors"
    Public Sub New()
        MyBase.New()
    End Sub
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub
    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Methods"
    'Public Sub InitRandomPoints(ByVal maxX As Double, ByVal maxY As Double, ByVal cnt As Int32)
    '    For i As Int32 = 0 To cnt - 1
    '        Me.Add(Endpoint.RandomPoint(maxX, maxY))
    '    Next
    'End Sub
    Public Sub MergeEquivalentPoints(ByVal maxX As Double, ByVal maxY As Double)
        Dim cnt As Int32 = Me.Count
        For i As Int32 = 0 To cnt - 1
            Dim ep1 As Endpoint = Me(i)
            For j As Int32 = i + 1 To cnt - 1
                Dim ep2 As Endpoint = Me(j)
                Endpoint.MergeEquivalentPoints(Me(i), Me(j), maxX, maxY)
            Next
        Next
    End Sub
    Public Sub UpdateEndpointsRoadSegments()
        Dim rscnt As Int32 = ObjectsModule.RoadSegments.Count
        For i As Int32 = 0 To rscnt - 1
            Dim rs As RoadSegment = ObjectsModule.RoadSegments(i)
            Dim ep1 As Endpoint = rs.Endpoint1
            Dim ep2 As Endpoint = rs.Endpoint2
            If Not ep1.RoadSegments.Contains(rs) Then ep1.RoadSegments.Add(rs)
            If Not ep2.RoadSegments.Contains(rs) Then ep1.RoadSegments.Add(rs)
        Next
    End Sub
#End Region
End Class
