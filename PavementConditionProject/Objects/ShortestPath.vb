﻿Public Module ShortestPath
    Private rank As Integer = 0
    Private baseLength As Double(,)
    Private LastPoint As Integer(,)
    Public ShortestDistance As Double(,)
    Public Solved As Boolean()
    Private trank As Integer = 0
    Public IsInit As Boolean = False
    Public Sub Update(Optional ByVal useCost As Boolean = False)
        ObjectsModule.Commuters.ClearCurrentBestRoutes()

        rank = ObjectsModule.Endpoints.Count

        baseLength = New Double(rank - 1, rank - 1) {}
        ''Init for not connected
        For i As Integer = 0 To rank - 1
            For j As Integer = 0 To rank - 1
                baseLength(i, j) = -1
            Next
        Next

        ''Init for Equivalent
        For i As Integer = 0 To rank - 1
            Dim epi As Endpoint = ObjectsModule.Endpoints(i)
            Dim cnt As Int32 = epi.EquivalentPoints.Count

            For j As Integer = 0 To cnt - 1
                Dim k As Int32 = ObjectsModule.Endpoints.IndexOf(epi.EquivalentPoints(j))
                baseLength(i, k) = 0
                baseLength(k, i) = 0
            Next
        Next

        ''Set Segment Lengths
        Dim cntSeg As Int32 = ObjectsModule.RoadSegments.Count
        For i As Integer = 0 To cntSeg - 1
            Dim rs As RoadSegment = ObjectsModule.RoadSegments(i)
            Dim e1 As Int32 = ObjectsModule.Endpoints.IndexOf(rs.Endpoint1)
            Dim e2 As Int32 = ObjectsModule.Endpoints.IndexOf(rs.Endpoint2)

            If useCost Then
                baseLength(e1, e2) = rs.SegmentCost
                baseLength(e2, e1) = rs.SegmentCost
            Else
                baseLength(e1, e2) = rs.SegmentLength
                baseLength(e2, e1) = rs.SegmentLength
            End If
        Next

        Solved = New Boolean(rank - 1) {}
        LastPoint = New Integer(rank - 1, rank - 1) {}
        ShortestDistance = New Double(rank - 1, rank - 1) {}
        For i As Integer = 0 To rank - 1
            Solved(i) = False
            For j As Integer = 0 To rank - 1
                ShortestDistance(i, j) = -1
                LastPoint(i, j) = -1
            Next
        Next
        IsInit = True
    End Sub
    Public Function InitSolving(ByVal Origin As Int32) As Boolean
        Dim cnt As Int32 = 0
        For j As Integer = 0 To rank - 1
            ShortestDistance(Origin, j) = -1
            LastPoint(Origin, j) = -1
        Next
        For i As Int32 = 0 To rank - 1
            Dim length As Double = baseLength(Origin, i)
            If length >= 0 Then
                ShortestDistance(Origin, i) = baseLength(Origin, i)
                LastPoint(Origin, i) = Origin
                cnt += 1
            End If
        Next
        Return (cnt > 0)
    End Function
    Public Function Solving(ByVal Origin As Int32) As Boolean
        Dim cnt As Int32 = 0
        For i As Integer = 0 To rank - 1
            Dim oldLen As Double = ShortestDistance(Origin, i)
            For j As Integer = 0 To rank - 1
                If i = j Then Continue For
                If ShortestDistance(Origin, j) < 0 Then Continue For
                If baseLength(i, j) < 0 Then Continue For
                Dim newLen As Double = ShortestDistance(Origin, j) + baseLength(i, j)
                If oldLen < 0 Or newLen < oldLen Then
                    ShortestDistance(Origin, i) = newLen
                    LastPoint(Origin, i) = j
                    cnt += 1
                End If
            Next
        Next
        Return (cnt > 0)
    End Function
    Public Sub Run(ByVal Origin As Int32)
        Dim str As String = ""

        If Not InitSolving(Origin) Then Return


        While 1
            str += "iteration" & trank & vbCrLf
            For i As Integer = 0 To rank - 1
                str += ShortestDistance(Origin, i) & " "
            Next
            str += vbCrLf
            For i As Integer = 0 To rank - 1
                str += (LastPoint(Origin, i) & " ")
            Next
            str += vbCrLf
            trank += 1
            If Not Solving(Origin) Then Exit While

        End While
        Solved(Origin) = True
    End Sub
    Public ReadOnly Property ShortestPath(ByVal Origin As Int32, ByVal Destination As Int32) As String
        Get
            If Not Solved(Origin) Then Run(Origin)
            ShortestPath = "" 'Destination.ToString
            Dim lastdest As Int32 = Destination
            Dim lastE As Endpoint = ObjectsModule.Endpoints(Destination)
            While 1
                Dim newDest As Int32 = LastPoint(Origin, lastdest)
                If newDest = lastdest Then
                    Exit While
                End If
                Dim newE As Endpoint = ObjectsModule.Endpoints(newDest)

                If newE.EquivalentPoints.Contains(lastE) Then
                    lastdest = newDest
                    lastE = newE
                    Continue While
                End If
                Dim cnt As Int32 = lastE.RoadSegments.Count
                For i As Int32 = 0 To cnt - 1
                    Dim rs1 As RoadSegment = lastE.RoadSegments(i)
                    If (rs1.Endpoint1 Is lastE AndAlso rs1.Endpoint2 Is newE) _
                        OrElse (rs1.Endpoint2 Is lastE AndAlso rs1.Endpoint1 Is newE) _
                        Then
                        If ShortestPath = "" Then
                            ShortestPath = ObjectsModule.RoadSegments.IndexOf(rs1).ToString
                        Else
                            ShortestPath += "," + ObjectsModule.RoadSegments.IndexOf(rs1).ToString
                        End If
                        lastdest = newDest
                        lastE = newE
                        Continue While
                    End If
                Next
                Throw New Exception("Shortest Path Equation Failed.")
            End While

        End Get
    End Property
End Module
