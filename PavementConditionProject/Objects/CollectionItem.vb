﻿Public MustInherit Class CollectionItem
    Implements IDisposable
#Region "Constructors"
    Public Sub New()
    End Sub
#End Region
#Region "Destructors"
    Protected MustOverride Sub DisposeManaged()
    Protected MustOverride Sub DisposeUnmanaged()
#End Region
#Region "IDisposable Support"
    Private disposedValue As Boolean

    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                DisposeManaged()
            End If
            DisposeUnmanaged()
        End If
        Me.disposedValue = True
    End Sub
    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

End Class
