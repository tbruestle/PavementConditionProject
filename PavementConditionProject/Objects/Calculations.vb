﻿Module Calculations
    Dim m_iterationCount As Int32 = 0
    Public Sub Initialize()
        ObjectsModule.Routes.InitializeGrid()
        ObjectsModule.Commuters.RandomNew(10)
        ObjectsModule.Commuters.InitBestRoutes()
        m_iterationCount = 0
    End Sub
    Public Function UpdateDesiredRoute(ByVal c As Commuter) As Boolean
        ''Returns if a new route should be searched for

        If c.LastRouteHasConstruction Then Return True

        Return c.LastRouteLength < 2.0 * c.LastRouteCost
    End Function
    Public Sub Commute(ByVal c As Commuter)
        c.LastRouteCost = c.PathCost(c.LastRouteList)
        c.UpdateHistoricalCost()
    End Sub
    Public Sub UpdateTraffic()
        ObjectsModule.RoadSegments.ResetNumberOfCommutersThisIteration()
        Dim cnt As Int32 = ObjectsModule.Commuters.Count
        For i As Int32 = 0 To cnt - 1
            Dim c As Commuter = ObjectsModule.Commuters(i)
            Dim cntrs As Int32 = c.LastRouteList.Count
            For j As Int32 = 0 To cntrs - 1
                c.LastRouteList(j).NumberOfCommutersThisIteration += 1
            Next
        Next
    End Sub
    Public Function SegmentCost(ByVal rs As RoadSegment) As Double
        If rs.DaysUntilFixed > 0 Then
            Return rs.SegmentLength * 10
        End If
        SegmentCost = rs.SegmentLength * (1 + rs.RoadDamage / 100) * (1 + Convert.ToDouble(rs.NumberOfCommutersThisIteration) / 100)
    End Function
    Public Sub DamageRoadSegment(ByVal rs As RoadSegment)
        rs.RoadDamage += rs.NumberOfCommutersThisIteration
        rs.RoadDamage += 1
    End Sub
    Public Sub Iteration()
        m_iterationCount += 1
        ObjectsModule.Commuters.ClearCurrentBestRoutePathCosts()
        ObjectsModule.Commuters.ClearBestRoutePathCosts()
        ObjectsModule.RoadSegments.ResetNumberOfCommutersThisIteration()
        ObjectsModule.Commuters.UpdateDesiredRoutes()
        UpdateTraffic()
        ObjectsModule.Commuters.Commute()
        ObjectsModule.RoadSegments.DamageRoadSegments()
        If m_iterationCount > 10 Then
            RepairRoadSegments(budget:=10)
        End If
    End Sub
    Public ReadOnly Property IterationCount As Int32
        Get
            Return m_iterationCount
        End Get
    End Property
    Public Sub RepairRoadSegments(ByVal budget As Double)
        Dim segs = ObjectsModule.RoadSegments.ToList.OrderByDescending(Function(p) p.RepairPriority)
        Dim cnt As Int32 = segs.Count
        For i As Int32 = 0 To cnt - 1
            Dim seg As RoadSegment = segs(i)
            Dim cost As Double = seg.RepairCost
            If budget < cost Then Exit For
            If seg.RepairPriority = 0 Then Exit For

            budget -= cost
            seg.DaysUntilFixed = seg.RepairTime
        Next
        For i As Int32 = 0 To cnt - 1
            Dim seg As RoadSegment = segs(i)
            If seg.DaysUntilFixed > 0 Then
                seg.DaysUntilFixed -= 1
            End If
        Next
    End Sub
    Public ReadOnly Property RepairPriority(ByVal rs As RoadSegment) As Double
        Get
            If rs.RoadDamage = 0 Then Return 0
            If rs.DaysUntilFixed > 0 Then Return 0

            Return rs.RoadDamage * rs.NumberOfCommutersThisIteration
        End Get
    End Property
    Public ReadOnly Property RepairCost(ByVal rs As RoadSegment) As Double
        Get
            Return rs.SegmentLength / 125
        End Get
    End Property
    Public ReadOnly Property RepairTime(ByVal rs As RoadSegment) As Int32
        Get
            Return Math.Ceiling(rs.SegmentLength / 125)
        End Get
    End Property
End Module
