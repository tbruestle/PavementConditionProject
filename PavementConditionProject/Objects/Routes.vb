﻿Public Class Routes
    Inherits ItemCollection(Of Route)
#Region "Member Variables"
    Private m_xMax As Double
    Private m_yMax As Double
#End Region
#Region "Constructors"
    Public Sub New()
        MyBase.New()
        m_xMax = 500
        m_yMax = 500
        Randomize()
    End Sub
#End Region
#Region "Destructors"
    Protected Overrides Sub DisposeManaged()

    End Sub

    Protected Overrides Sub DisposeUnmanaged()

    End Sub
#End Region
#Region "Properties"
    Public ReadOnly Property RoadSegments As RoadSegments
        Get
            Return ObjectsModule.RoadSegments
        End Get
    End Property
#End Region
#Region "Methods"
    Public Sub InitializeHighways(ByVal verticalCount As Int32, ByVal horizontalCount As Int32)
        For i As Int32 = 1 To verticalCount
            Route.NewSpanningRoute(Me.RoadSegments, True, m_xMax, m_yMax, 6)
        Next
        For i As Int32 = 1 To horizontalCount
            Route.NewSpanningRoute(Me.RoadSegments, False, m_xMax, m_yMax, 6)
        Next
        RoadSegments.CreateOffshootsOfType(30, 100, 2)
        RoadSegments.CreateOffshootsOfType(30, 60, 3)
        RoadSegments.CreateOffshootsOfType(60, 60, 3)
        RoadSegments.SplitIntersections()
        RoadSegments.SplitOnEdgeCrossings(m_xMax, m_yMax)
    End Sub
    Public Sub InitializeGrid()
        Dim r1 As New Route(Me.RoadSegments, New Endpoint(0, m_yMax / 2), New Endpoint(m_xMax, m_yMax / 2), 2, 1)
        Dim r2 As New Route(Me.RoadSegments, New Endpoint(m_xMax / 2, 0), New Endpoint(m_xMax / 2, m_yMax), 2, 1)

        For i As Int32 = 1 To 3 Step 2
            Dim r1a As New Route(Me.RoadSegments, New Endpoint(0, m_yMax * i / 4), New Endpoint(m_xMax, m_yMax * i / 4), 2, 1)
            Dim r2a As New Route(Me.RoadSegments, New Endpoint(m_xMax * i / 4, 0), New Endpoint(m_xMax * i / 4, m_yMax), 2, 1)
        Next

        For i As Int32 = 1 To 7 Step 2
            Dim r1a As New Route(Me.RoadSegments, New Endpoint(0, m_yMax * i / 8), New Endpoint(m_xMax, m_yMax * i / 8), 2, 1)
            Dim r2a As New Route(Me.RoadSegments, New Endpoint(m_xMax * i / 8, 0), New Endpoint(m_xMax * i / 8, m_yMax), 2, 1)
        Next


        RoadSegments.SplitIntersections()
        RoadSegments.SplitOnEdgeCrossings(m_xMax, m_yMax)
        ObjectsModule.Endpoints.UpdateEndpointsRoadSegments()
        ObjectsModule.Endpoints.MergeEquivalentPoints(m_xMax, m_yMax)
    End Sub
#End Region
End Class
