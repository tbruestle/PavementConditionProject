﻿

Class MainWindow
    Dim routes As Routes

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Initialize()
    End Sub


    Public Sub Initialize()
        Calculations.Initialize()
        ObjectsModule.DrawAll(Me.drawCanvas, 500, 500)
        ObjectsModule.LoadVariables(Me)
    End Sub
    Public Sub Iteration()
        Calculations.Iteration()
    End Sub

    Private Sub btnIterate_Click_1(sender As Object, e As RoutedEventArgs)
        Iteration()
        ObjectsModule.DrawAll(Me.drawCanvas, 500, 500)
        ObjectsModule.LoadVariables(Me)
    End Sub
End Class
